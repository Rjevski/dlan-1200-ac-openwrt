#!/bin/bash
set -e

. config.sh

FIRMWARE_DIR='firmware'
SOURCE_DIR='openwrt'

git clone $FIRMWARE_REPO $FIRMWARE_DIR

cd $FIRMWARE_DIR
./get_firmware.sh
FEED_DIR=$(realpath feeds)
cd ..

git clone $OPENWRT_REPO $SOURCE_DIR

cd $SOURCE_DIR
git checkout $GIT_TAG

cp feeds.conf.default feeds.conf
echo "src-cpy dlan $FEED_DIR" >> feeds.conf

./scripts/feeds update -a
./scripts/feeds install -a

cp ../config .config
make defconfig
make download
make world -j $(nproc)
